package com.eclipsekingdom.botbank.api;

import com.eclipsekingdom.botbank.bot.Bot;
import com.eclipsekingdom.botbank.bot.BotCache;
import com.eclipsekingdom.botbank.bot.Launcher;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class BotBankApi {

    private static Map<UUID, CompletableFuture<LaunchedBot>> botToLaunchedBot = new HashMap<>();

    public static BotBankApi getInstance(DiscordApp discordApp) {
        return new BotBankApi(discordApp);
    }

    private DiscordApp discordApp;
    private com.eclipsekingdom.botbank.bot.Bot bot;

    private BotBankApi(DiscordApp discordApp) {
        this.discordApp = discordApp;
        this.bot = BotCache.getBot(discordApp.getBotName());
        if (bot != null) {
            bot.registerIntents(discordApp.getGatewayIntents());
            bot.registerFlags(discordApp.getCacheFlags());
            bot.registerPolicy(discordApp.getMemberCachePolicy());
            CompletableFuture<LaunchedBot> launchedBot = new CompletableFuture<>();
            botToLaunchedBot.put(bot.getId(), launchedBot);
        }
    }

    public boolean hasBot() {
        return bot != null;
    }

    public CompletableFuture<LaunchedBot> getLaunchedBot() {
        if (bot != null) {
            CompletableFuture<LaunchedBot> launchedBot = new CompletableFuture();
            CompletableFuture<Bot> botFuture = new CompletableFuture<>();
            botFuture.thenAccept(bot -> {
                launchedBot.complete(new LaunchedBot(bot, discordApp));
            });
            Launcher.registerBot(bot.getId(), discordApp.getNamespace(), botFuture);
            return launchedBot;
        } else {
            return null;
        }
    }

}
