package com.eclipsekingdom.botbank.bungee.plugin;

import com.eclipsekingdom.botbank.bungee.BungeeMain;
import com.eclipsekingdom.botbank.util.plugin.ConfigLoader;
import com.eclipsekingdom.botbank.util.plugin.IConsole;
import com.eclipsekingdom.botbank.util.plugin.IPlugin;
import com.eclipsekingdom.botbank.util.plugin.IScheduler;
import net.md_5.bungee.api.ProxyServer;

public class BungeePlugin implements IPlugin {

    private ProxyServer proxyServer = BungeeMain.getPlugin().getProxy();
    private BungeeScheduler scheduler = new BungeeScheduler();
    private String version = BungeeMain.getPlugin().getDescription().getVersion();
    private BungeeConfigLoader configLoader = new BungeeConfigLoader();
    private BungeeConsole console = new BungeeConsole();

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    @Override
    public IScheduler getScheduler() {
        return scheduler;
    }

    @Override
    public IConsole getConsole() {
        return console;
    }

    @Override
    public int getOnlinePlayerCount() {
        return proxyServer.getOnlineCount();
    }

}
