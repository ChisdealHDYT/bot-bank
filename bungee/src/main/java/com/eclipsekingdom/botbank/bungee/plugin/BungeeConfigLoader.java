package com.eclipsekingdom.botbank.bungee.plugin;

import com.eclipsekingdom.botbank.util.plugin.ConfigLoader;
import com.eclipsekingdom.botbank.util.plugin.FileConfig;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class BungeeConfigLoader implements ConfigLoader {

    @Override
    public FileConfig load(File file) {
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            Configuration configuration = YamlConfiguration.getProvider(YamlConfiguration.class).load(file);
            return new BungeeConfig(configuration);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
