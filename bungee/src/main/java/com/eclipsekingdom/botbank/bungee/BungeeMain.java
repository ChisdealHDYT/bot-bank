package com.eclipsekingdom.botbank.bungee;

import com.eclipsekingdom.botbank.BotBank;
import com.eclipsekingdom.botbank.BotBankListener;
import com.eclipsekingdom.botbank.bungee.plugin.BungeePlugin;
import com.eclipsekingdom.botbank.config.PluginConfig;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.TimeUnit;

public final class BungeeMain extends Plugin {

    private static BungeeMain plugin;

    @Override
    public void onEnable() {
        plugin = this;
        BotBank.startup(new BungeePlugin());
        new BungeeListener();
        getProxy().getScheduler().schedule(this, () -> {
            BotBankListener.onLoad();
        }, PluginConfig.getAppRegistrationPeriod(), TimeUnit.SECONDS);
    }

    @Override
    public void onDisable() {
        BotBank.shutdown();
    }

    public static BungeeMain getPlugin() {
        return plugin;
    }

}
