# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [2.0.1] - 2020/11/11

#### Fixed 

- bot registration close method for legacy servers

## [2.0.0] - 2020/11/11

#### Added

- Changelog
- Support for new Discord Api gateway intents
- Added bot command registrar

#### Changed

- Plugins using Bot Bank now need to register intents, cache flags, and member cache policy
- Bots are now launched async
- Api updated
- Error messages are more user-friendly

#### Removed

- intents from bots.yml

  
<!--
Added - for new features.
Changed - for changes in existing functionality.
Deprecated - for soon-to-be removed features.
Removed - for now removed features.
Fixed - for any bug fixes.
Security - in case of vulnerabilities. 
 -->