<!-- PROJECT SHIELDS -->
[![Version][version-shield]][version-url]
[![Download][download-shield]][download-url]
[![Tested Versions][tested-shield]][tested-url]
[![MIT License][license-shield]][license-url]
[![Discord][discord-shield]][discord-url]

<!-- PROJECT LOGO -->
<br/>
<div align="center">

  [![Discord Link](images/logo.jpg)](https://gitlab.com/sword7/bot-bank)

  ### Bot Bank <br><sub>*Including JDA in plugin form*</sub>
  **[Explore the docs »](https://gitlab.com/sword7/bot-bank)**
  <br/>
  [Report Bug](https://discord.com/invite/hKTXQBH)
  ·
  [Request Feature](https://discord.com/invite/hKTXQBH)
  
</div>

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Downloads](#downloads)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
  * [Registering app](#registering-app)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<!-- ABOUT THE PROJECT -->
## About The Project

![Discord Link Screen Shot](images/cover.png)


Bot Bank is a bot management API. It allows multiple Spigot plugins to use the same bot's JDA connection, preventing redundancy.

The Bot Bank jar also compiles the JDA jar. JDA stands for Java Discord API. It is a Java wrapper of the Discord API developed by dv8tion. 
Because Bot Bank is a Spigot and BungeeCord plugin, it allows other Spigot and BungeeCord plugins to easily access the JDA.

#### Built With

* [BungeeCord](https://www.spigotmc.org/wiki/bungeecord/)
* [JDA](https://github.com/DV8FromTheWorld/JDA)
* [Spigot](https://www.spigotmc.org/wiki/spigot/)

<!-- GETTING STARTED -->
## Getting Started

#### downloads

The latest Spigot version of Bot Bank Link is available on [SpigotMC](https://www.spigotmc.org/resources/bot-bank.72682/).

All versions: 

- [Spigot](https://eclipsekingdom.com/plugins/resources/botbank/spigot/BotBank.jar)
- [BungeeCord](https://eclipsekingdom.com/plugins/resources/botbank/bungee/BotBank.jar)
- [API](https://eclipsekingdom.com/plugins/resources/botbank/api/BotBank-api.jar)

#### Prerequisites

![Tested Versions][tested-shield]

Bot Bank requires a Minecraft server running version 1.8 to 1.6.

#### Installation

Install Bot Bank like any other Spigot or Bungee plugin. If you need a refresher you can refer to [Installation](https://gitlab.com/sword7/bot-bank/-/wikis/setup/installation).

<!-- USAGE EXAMPLES -->
## Usage

#### Registering App

The Bot Bank API can only be accessed by declaring and registering a DiscordApp. 
The DiscordApp object consists of a plugin name, target bot name, bot intents, cache flags, and member cache policy.

```
List<GatewayIntent> intents = new ArrayList<>();
intents.add(GatewayIntent.GUILD_MESSAGES);
intents.add(GatewayIntent.GUILD_MESSAGE_REACTIONS);
DiscordApp app = new DiscordApp("MyPlugin", PluginConfig.getBotName(), 
intents, Collections.EMPTY_LIST, MemberCachePolicy.NONE);
//get BotBank API
BotBankApi botBankAPI = BotBankApi.getInstance(app);
```

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are greatly appreciated.

See `CONTRIBUTING` for more information.

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- CONTACT -->
## Contact

sword7 - sword#5439 - devsword7@gmail.com

Project Link: [https://gitlab.com/sword7/bot-bank](https://gitlab.com/sword7/bot-bank)

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

Link icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>

* [GitLab](https://gitlab.com)
* [JetBrains](https://www.jetbrains.com/?from=EclipseKingdom)
* [Shields IO](https://shields.io/)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[version-shield]: https://img.shields.io/spiget/version/72684?label=&labelColor=b487b8&color=d9a7de
[version-url]: https://www.spigotmc.org/resources/bot-bank.72682/
[download-shield]: https://img.shields.io/spiget/downloads/72684?&color=efb61c&style=flat-square&logo=image%2Fx-icon%3Bbase64%2CAAABAAEAEBAQAAAAAAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAND%2FAOhGOgA%2F6OIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAiAAAAAAAAACIAAAAAAAAAIgAAAAAAAAAAAAAAAAAAABEAAAAzMQABEQAAARMzEBERARERETMxERAAAAARMzEAAAAAAAETMwAAAAAAABEwAAAAAAAAERAAAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAD%2F%2BQAA%2F%2FkAAP%2F5AAD%2F8AAA%2BDAAAPAgAAAAAAAAAAEAAAADAADwDwAA%2FB8AAPwfAAD8HwAA%2Fj8AAP4%2FAADwBwAA
[download-url]: https://www.spigotmc.org/resources/bot-bank.72682/
[license-shield]: https://img.shields.io/badge/license-MIT-blue?style=flat-square
[tested-shield]: https://img.shields.io/spiget/tested-versions/72684?style=flat-square
[tested-url]: https://www.spigotmc.org/resources/bot-bank.72682/
[license-url]: https://gitlab.com/sword7/bot-bank/-/blob/master/LICENSE.txt
[discord-shield]: https://img.shields.io/discord/623658924079448074?label=&style=flat&labelColor=697ec4&color=8196de&logoColor=ffffff&logo=Discord&logoWidth=20
[discord-url]: https://discord.com/invite/hKTXQBH
