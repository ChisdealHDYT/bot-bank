package com.eclipsekingdom.botbank.spigot.plugin;

import com.eclipsekingdom.botbank.util.plugin.IConsole;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;

public class SpigotConsole implements IConsole {

    private ConsoleCommandSender consoleSender = Bukkit.getConsoleSender();

    @Override
    public void raw(String s) {
        consoleSender.sendMessage(s);
    }

    @Override
    public void info(String s) {
        raw("[BotBank] " + s);
    }

    @Override
    public void warn(String s) {
        raw("[BotBank] [WARN] " + s);
    }

}
