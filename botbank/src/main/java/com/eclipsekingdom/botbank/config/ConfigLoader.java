package com.eclipsekingdom.botbank.config;

import com.eclipsekingdom.botbank.BotBank;
import com.google.common.collect.ImmutableList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class ConfigLoader {

    private static final String pluginFolder = "BotBank";

    private static ImmutableList<String> configs = new ImmutableList.Builder<String>()
            .add("bots")
            .add("config")
            .build();

    public static void load() {
        try {
            for (String config : configs) {
                File target = new File("plugins/" + pluginFolder, config + ".yml");
                if (!target.exists()) {
                    load("config/" + config + ".yml", target);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void load(String resource, File file) throws IOException {
        file.getParentFile().mkdirs();
        InputStream in = BotBank.getResource("/" + resource);
        Files.copy(in, file.toPath());
        in.close();
    }


}
