package com.eclipsekingdom.botbank.util.plugin;

public class Scheduler {

    private static IScheduler scheduler = Plugin.getScheduler();

    public static void runAsync(Runnable r) {
        scheduler.runAsync(r);
    }

}
