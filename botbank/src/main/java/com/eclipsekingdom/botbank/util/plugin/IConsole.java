package com.eclipsekingdom.botbank.util.plugin;

public interface IConsole {

    void raw(String s);

    void info(String s);

    void warn(String s);

}
