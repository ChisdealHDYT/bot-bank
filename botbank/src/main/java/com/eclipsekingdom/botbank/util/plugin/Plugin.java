package com.eclipsekingdom.botbank.util.plugin;

public class Plugin {

    private static IPlugin plugin;

    public static void init(IPlugin toSet) {
        plugin = toSet;
    }

    public static String getVersion() {
        return plugin.getVersion();
    }

    public static ConfigLoader getConfigLoader() {
        return plugin.getConfigLoader();
    }

    public static IScheduler getScheduler() {
        return plugin.getScheduler();
    }

    public static int getOnlinePlayerCount() {
        return plugin.getOnlinePlayerCount();
    }

}
