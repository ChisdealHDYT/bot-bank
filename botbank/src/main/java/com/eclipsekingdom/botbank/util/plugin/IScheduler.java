package com.eclipsekingdom.botbank.util.plugin;

public interface IScheduler {

    void runAsync(Runnable r);

}
