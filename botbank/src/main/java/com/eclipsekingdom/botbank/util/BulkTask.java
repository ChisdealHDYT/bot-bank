package com.eclipsekingdom.botbank.util;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public abstract class BulkTask extends TimerTask {

    protected static Timer timer = new Timer();

    private State state;
    private int extensions = 0;
    private int duration;
    private int countdown;
    private int maxExtensions;

    public BulkTask(int duration, int maxExtensions) {
        this.state = State.RESTING;
        this.duration = duration;
        this.countdown = duration;
        this.maxExtensions = maxExtensions;
        timer.schedule(this, TimeUnit.SECONDS.toMillis(1), TimeUnit.SECONDS.toMillis(1));
    }

    @Override
    public void run() {
        if (state != State.RESTING) {
            countdown--;
            if (countdown <= 0) {
                runTask();
                reset();
            }
        }
    }

    private void reset() {
        this.state = State.RESTING;
        this.countdown = duration;
        this.extensions = 0;
    }

    protected abstract void runTask();

    public void onExtend() {
        state = State.COMBINING;
        countdown = duration;
        extensions++;
        if (extensions == maxExtensions) {
            runTask();
        }
    }

    public enum State {
        RESTING,
        COMBINING,
    }

}