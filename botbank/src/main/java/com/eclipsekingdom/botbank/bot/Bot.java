package com.eclipsekingdom.botbank.bot;

import com.eclipsekingdom.botbank.config.PluginConfig;
import com.eclipsekingdom.botbank.server.Server;
import com.eclipsekingdom.botbank.util.plugin.Console;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Bot {

    private JDA jda;
    private UUID id = UUID.randomUUID();
    private BotStatus status;
    private String name;
    protected String token;
    private String commandPrefix;
    private BotPresence presence;
    private Set<GatewayIntent> gatewayIntents = new HashSet<>();
    private Set<CacheFlag> cacheFlags = new HashSet<>();
    private MemberCachePolicy memberCachePolicy = MemberCachePolicy.NONE;

    public Bot(String name, String token, String commandPrefix, BotPresence presence) {
        this.name = name;
        this.token = token;
        this.status = BotStatus.UNINITIALIZED;
        this.commandPrefix = commandPrefix;
        this.presence = presence;
    }

    public void launch() {
        try {
            jda = JDABuilder.create(token, gatewayIntents).enableCache(cacheFlags).disableCache(getInverseFlags()).setMemberCachePolicy(memberCachePolicy).build();
            try {
                int timeout = PluginConfig.getBotConnectionTimeout() * 1000;
                int currentTime = 0;
                boolean timedOut = true;
                while (jda.getStatus() != JDA.Status.CONNECTED) {
                    if (jda.getStatus() != JDA.Status.SHUTDOWN) {
                        TimeUnit.MILLISECONDS.sleep(100);
                        currentTime += 100;
                        if (currentTime % 1000 == 0) sendConsole(jda.getStatus().toString() + "...");
                        if (currentTime > timeout) break;
                    } else {
                        abort();
                        timedOut = false;
                        break;
                    }
                }
                if (jda.getStatus() == JDA.Status.CONNECTED) {
                    onEnable();
                } else if (timedOut) {
                    abort("Connection timout");
                }
            } catch (Exception e) {
                abort(e.getMessage());
            }
        } catch (Exception e) {
            abort(e.getMessage());
        }
        sendConsole(status.getMessage());
    }

    private void abort(String message) {
        abort();
        sendConsole(message);
    }

    private void abort() {
        if (jda != null) jda.shutdown();
        status = BotStatus.CONNECTION_FAILED;
    }

    private void onEnable() {
        jda.setAutoReconnect(true);
        status = BotStatus.ONLINE;
        updatePresence();
    }

    public void shutdown() {
        if (jda != null) {
            try {
                jda.shutdownNow();
            } catch (Exception e) {
                sendConsole(e.getMessage());
            }
        }
    }

    private void sendConsole(String s) {
        Console.info("[" + name + "] " + s);
    }

    public void updatePresence() {
        if (presence.isEnabled()) {
            String effectiveMessage = presence.getMessage()
                    .replaceAll("%online%", String.valueOf(Server.getOnlinePlayers()));
            jda.getPresence().setActivity(Activity.of(presence.getType(), effectiveMessage));
        }
    }

    public JDA getJDA() {
        return jda;
    }

    public String getName() {
        return name;
    }

    public BotPresence getPresence() {
        return presence;
    }

    public BotStatus getStatus() {
        return status;
    }

    public void registerIntents(Collection<GatewayIntent> gatewayIntents) {
        this.gatewayIntents.addAll(gatewayIntents);
    }

    public void registerFlags(Collection<CacheFlag> cacheFlags) {
        this.cacheFlags.addAll(cacheFlags);
    }

    public void registerPolicy(MemberCachePolicy memberCachePolicy) {
        this.memberCachePolicy = this.memberCachePolicy.and(memberCachePolicy);
    }

    public UUID getId() {
        return id;
    }

    public String getCommandPrefix() {
        return commandPrefix;
    }

    private Set<CacheFlag> getInverseFlags() {
        Set<CacheFlag> inverseFlags = new HashSet<>();
        for (CacheFlag cacheFlag : CacheFlag.values()) {
            if (!cacheFlags.contains(cacheFlag)) {
                inverseFlags.add(cacheFlag);
            }
        }
        return inverseFlags;
    }


}
