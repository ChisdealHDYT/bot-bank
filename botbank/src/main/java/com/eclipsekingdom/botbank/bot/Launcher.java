package com.eclipsekingdom.botbank.bot;

import com.eclipsekingdom.botbank.util.plugin.Scheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class Launcher {

    public static Map<UUID, Map<String, CompletableFuture<Bot>>> botToLaunchedBot = new HashMap<>();

    public static void registerBot(UUID botId, String namespace, CompletableFuture<Bot> botFuture) {
        if (botToLaunchedBot.containsKey(botId)) {
            botToLaunchedBot.get(botId).put(namespace, botFuture);
        } else {
            Map<String, CompletableFuture<Bot>> appToLaunchedBot = new HashMap<>();
            appToLaunchedBot.put(namespace, botFuture);
            botToLaunchedBot.put(botId, appToLaunchedBot);
        }
    }

    public static boolean launched = false;

    public static void launch() {
        launched = true;
        Scheduler.runAsync(() -> {
            for (Bot bot : BotCache.getBots()) {
                bot.launch();
                UUID botId = bot.getId();
                if (botToLaunchedBot.containsKey(botId)) {
                    for (Map.Entry<String, CompletableFuture<Bot>> entry : botToLaunchedBot.get(botId).entrySet()) {
                        CompletableFuture<Bot> launchedBot = entry.getValue();
                        launchedBot.complete(bot);
                    }
                }
            }
        });
    }

    public static boolean isLaunched() {
        return launched;
    }

}
