package com.eclipsekingdom.botbank.bot;

public enum BotStatus {

    ONLINE("Online"),
    OFFLINE("Offline"),
    CONNECTION_FAILED("Connection Failed"),
    UNINITIALIZED("Uninitialized"),
    LAUNCHING("Launching"),
    ;

    private String message;

    BotStatus(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public boolean isOnline() {
        return this == ONLINE;
    }

}